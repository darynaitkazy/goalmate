package com.goal.goalmate.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.UUID;

@Entity
@Table(name="contracts")
@Getter
@Setter
public class Contract {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    private String title;
    private String description;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private String reward;
    private String punishment;
    private String friendName;
    private String friendNumber;
    private UUID userId;
    private ContractStatus contractStatus;
    private String fullName;
    private Integer photoId;

    public enum ContractStatus {
        ONGOING,
        DONE,
        ARCHIVED
    }
}
