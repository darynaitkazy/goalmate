package com.goal.goalmate.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;
@Entity
@Table(name = "followings")
@Getter
@Setter
public class Follow {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID id;
    @ManyToOne
    private User follower;
    @ManyToOne
    private User followed;
}
