package com.goal.goalmate.payload.response;

import java.util.List;
import java.util.UUID;

public class UserInfoResponse {
	private UUID id;
	private String username;
	private String email;
	private String firstname;
	private String lastname;
	private List<String> roles;

	private String token;

	public UserInfoResponse(UUID id, String username, String email, List<String> roles, String token,
							String firstname, String lastname) {
		this.id = id;
		this.username = username;
		this.email = email;
		this.roles = roles;
		this.token = token;
		this.firstname = firstname;
		this.lastname = lastname;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<String> getRoles() {
		return roles;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
}
