package com.goal.goalmate.controllers;

import com.goal.goalmate.models.Contract;
import com.goal.goalmate.repositories.ContractRepository;
import com.goal.goalmate.repositories.UserRepository;
import com.goal.goalmate.services.ContractService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/contracts")
public class ContractController {
    private final ContractRepository contractRepository;
    private final UserRepository userRepository;
    private final ContractService contractService;

    public ContractController(ContractRepository contractRepository, UserRepository userRepository, ContractService contractService) {
        this.contractRepository = contractRepository;
        this.userRepository = userRepository;
        this.contractService = contractService;
    }

    @PostMapping("/create")
    public ResponseEntity<?> createContract(@RequestBody Contract contract,
                                            @RequestParam (value = "userId", required = false) String userId) {
//        Authentication authentication = SecurityContextHolder .getContext().getAuthentication();
//        String userName = (String) authentication.getName();
//        Optional<User> user = userRepository.findByUsername(userName);
//        if (user.isPresent()) {
//            contract.setUserId(UUID.fromString(user.get().getId().toString()));
//        } else {
//            return ResponseEntity.notFound().build();
//        }
        contract.setUserId(UUID.fromString(userId));
        contract.setContractStatus(Contract.ContractStatus.ONGOING);
        contractRepository.save(contract);
        return ResponseEntity.ok(contract);
    }
    @GetMapping("")
    public ResponseEntity<?> getContracts(@RequestParam(value = "status", required = false) String status) {
        List<Contract> contracts = new ArrayList<>();
        if (status != null) {
            contracts = contractService.getContractsByStatus(status);
        } else {
            contracts = contractRepository.findAll();
        }
        return ResponseEntity.ok(contracts);
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getContractById(@PathVariable(value = "id") UUID contractId) {
        Optional<Contract> contract = contractRepository.findById(contractId);
        if (contract.isPresent()) {
            return ResponseEntity.ok(contract.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @GetMapping("/users/{userId}")
    public ResponseEntity<?> getContractsByUserId(@PathVariable UUID userId,
                                                  @RequestParam(value = "status", required = false) String status) {
        List<Contract> contracts = new ArrayList<>();
        List<Contract> currentContracts = contractRepository.findByUserId(userId);
        if (status != null) {
            contracts = contractService.getContractsByStatusAndUserId(status, currentContracts);
        }
        else contracts = contractRepository.findByUserId(userId);
        return ResponseEntity.ok(contracts);
    }
    @PutMapping("/{id}")
    public ResponseEntity<?> updateContract(@PathVariable(value="id") UUID contractId, @RequestBody Contract contract) {
        Optional<Contract> contractOpt = contractRepository.findById(contractId);
        if (contractOpt.isPresent()) {
            Contract existingContract = contractOpt.get();
            if (contract.getTitle() != null) {
                existingContract.setTitle(contract.getTitle());
            } if (contract.getDescription() != null) {
                existingContract.setDescription(contract.getDescription());
            } if (contract.getDateFrom() != null) {
                existingContract.setDateFrom(contract.getDateFrom());
            } if (contract.getDateTo() != null) {
                existingContract.setDateTo(contract.getDateTo());
            } if (contract.getReward() != null) {
                existingContract.setReward(contract.getReward());
            } if (contract.getPunishment() != null) {
                existingContract.setPunishment(contract.getPunishment());
            } if (contract.getFriendName() != null) {
                existingContract.setFriendName(contract.getFriendName());
            } if (contract.getFriendNumber() != null) {
                existingContract.setFriendNumber(contract.getFriendNumber());
            }
            contractRepository.save(existingContract);
            return ResponseEntity.ok(existingContract);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteContract(@PathVariable(value="id") UUID contractId) {
        Optional<Contract> contract = contractRepository.findById(contractId);
        if (contract.isPresent()) {
            contractRepository.delete(contract.get());
            return ResponseEntity.ok(contract);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    //endpoints for changing contract status
    @PutMapping("/{id}/change-status")
    public ResponseEntity<?> changeContractStatus(@PathVariable(value = "id") UUID id,
                                                  @RequestParam(value = "status") String status) {
        Optional<Contract> contractOpt = contractRepository.findById(id);
        if (contractOpt.isPresent()) {
            Contract contract = contractOpt.get();
            if (status.equals("ONGOING")) contract.setContractStatus(Contract.ContractStatus.ONGOING);
            if (status.equals("DONE")) contract.setContractStatus(Contract.ContractStatus.DONE);
            if (status.equals("ARCHIVED")) contract.setContractStatus(Contract.ContractStatus.ARCHIVED);
            contractRepository.save(contract);
            return ResponseEntity.ok(contract);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("{id}/join-contract")
    public ResponseEntity<?> joinContract(@PathVariable(value = "id") UUID id,
                                          @RequestParam(value = "userId") String userId) {
        Contract contract = contractRepository.findById(id).get();
        Contract newContract = new Contract();
        newContract.setDateFrom(contract.getDateFrom());
        newContract.setDateTo(contract.getDateTo());
        newContract.setDescription(contract.getDescription());
        newContract.setTitle(contract.getTitle());
        newContract.setFriendName(contract.getFriendName());
        newContract.setFriendNumber(contract.getFriendNumber());
        newContract.setReward(contract.getReward());
        newContract.setPunishment(contract.getPunishment());
        newContract.setUserId(UUID.fromString(userId));
        newContract.setContractStatus(Contract.ContractStatus.ARCHIVED);
        newContract.setPhotoId(contract.getPhotoId());
        newContract.setFullName(contract.getFullName());
        contractRepository.save(newContract);
        return ResponseEntity.ok(newContract);
    }
}
