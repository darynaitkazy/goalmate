package com.goal.goalmate.controllers;

import com.goal.goalmate.models.Contract;
import com.goal.goalmate.models.User;
import com.goal.goalmate.repositories.ContractRepository;
import com.goal.goalmate.repositories.UserRepository;
import com.goal.goalmate.services.FollowingService;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import static com.fasterxml.jackson.databind.type.LogicalType.Collection;

@RestController
@RequestMapping("/api/v1")
public class MainController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private FollowingService followingService;
    @Autowired
    private ContractRepository contractRepository;

    //GET api for getting contracts of a users you followed
    @GetMapping("/main")
    public ResponseEntity<?> getContractsOfFollowings(@RequestParam(value = "userId") String userId) {
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        String userName = (String) authentication.getName();
//        Optional<User> user = userRepository.findByUsername(userName);
        Optional<User>user = userRepository.findById(UUID.fromString(userId));
        List<User> followings = new ArrayList<>();
        List<Contract> contracts = new ArrayList<>();
        if (user.isPresent()) {
            followings = followingService.findFollowingUsers(user.get());
        }
        for (User u: followings) {
            List<Contract> c = contractRepository.findByUserId(u.getId());
            for (Contract cur: c) {
                if (cur.getContractStatus().equals(Contract.ContractStatus.ONGOING)) {
                    contracts.add(cur);
                }
            }
        }
        return ResponseEntity.ok(contracts);
    }

    @GetMapping("/random-users")
    public ResponseEntity<?> getRandomUsers(@RequestParam(value = "userId") String userId) {
        List<User> allUsers = userRepository.findAll();
        Collections.shuffle(allUsers);
        List<User> randomUsers = new ArrayList<>();
        for (User u: allUsers) {
            if (!u.getId().equals(UUID.fromString(userId))) {
                randomUsers.add(u);
            }
            if (randomUsers.size() == 5) break;
        }
        return ResponseEntity.ok(randomUsers);
    }
}
