package com.goal.goalmate.controllers;

import com.goal.goalmate.models.User;
import com.goal.goalmate.payload.request.ChangePasswordRequest;
import com.goal.goalmate.payload.response.MessageResponse;
import com.goal.goalmate.repositories.UserRepository;
import io.jsonwebtoken.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UserController(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @GetMapping("/")
    public ResponseEntity<?> getUsers() {
        return ResponseEntity.ok(userRepository.findAll());
    }

    @PostMapping("/change-password")
    public ResponseEntity<?> changePassword(@RequestParam(value = "userId") String userId,
            @RequestBody ChangePasswordRequest changePasswordRequest) throws IOException {
        // Retrieve the user from the database
        Optional<User> userOpt = userRepository.findById(UUID.fromString(userId));
        User user = userOpt.get();
        if (user == null) {
            return ResponseEntity.badRequest().body(new MessageResponse("Current password is incorrect"));
        }
        // Verify the current password
        if (!passwordEncoder.matches(changePasswordRequest.getCurrentPassword(), user.getPassword())) {
            return ResponseEntity.badRequest().body(new MessageResponse("Current password is incorrect"));
        }

        // Update the password
        user.setPassword(passwordEncoder.encode(changePasswordRequest.getNewPassword()));
        userRepository.save(user);
        return ResponseEntity.ok(user);
    }
}
