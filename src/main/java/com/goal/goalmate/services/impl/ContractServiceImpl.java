package com.goal.goalmate.services.impl;

import com.goal.goalmate.models.Contract;
import com.goal.goalmate.repositories.ContractRepository;
import com.goal.goalmate.services.ContractService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContractServiceImpl implements ContractService {
    private final ContractRepository contractRepository;

    public ContractServiceImpl(ContractRepository contractRepository) {
        this.contractRepository = contractRepository;
    }

    @Override
    public List<Contract> getContractsByStatus(String status) {
        List<Contract> currentContracts = contractRepository.findAll();
        List<Contract> contracts = new ArrayList<>();
        for (Contract contract: currentContracts) {
            if (contract.getContractStatus() == null) continue;
            Contract.ContractStatus contractStatus;
            if (status.equals("DONE")) contractStatus= Contract.ContractStatus.DONE;
            else if (status.equals("ONGOING")) contractStatus = Contract.ContractStatus.ONGOING;
            else contractStatus = Contract.ContractStatus.ARCHIVED;
            if (contract.getContractStatus().equals(contractStatus)) {
                contracts.add(contract);
            }
        }
        return contracts;
    }

    @Override
    public List<Contract> getContractsByStatusAndUserId(String status, List<Contract> currentContracts) {
        List<Contract> contracts = new ArrayList<>();
        for (Contract contract: currentContracts) {
            if (contract.getContractStatus() == null) continue;
            Contract.ContractStatus contractStatus;
            if (status.equals("DONE")) contractStatus= Contract.ContractStatus.DONE;
            else if (status.equals("ONGOING")) contractStatus = Contract.ContractStatus.ONGOING;
            else contractStatus = Contract.ContractStatus.ARCHIVED;
            if (contract.getContractStatus().equals(contractStatus)) {
                contracts.add(contract);
            }
        }
        return contracts;
    }
}
