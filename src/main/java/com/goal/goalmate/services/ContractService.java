package com.goal.goalmate.services;

import com.goal.goalmate.models.Contract;

import java.util.List;

public interface ContractService {
    List<Contract> getContractsByStatus(String status);
    List<Contract> getContractsByStatusAndUserId(String status, List<Contract> currentContracts);
}
