package com.goal.goalmate.services;

import com.goal.goalmate.models.Contract;
import com.goal.goalmate.models.Follow;
import com.goal.goalmate.models.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

public interface FollowingService {
    Follow createFollowing(User follower, User followed);
    void deleteFollowing(User follower, User followed);

    boolean checkFollowing(User follower, User followed);

    List<User> findFollowingUsers(User follower);

    List<User> findFollowedUsers(User followed);

    List<Contract> getContractsOfFollwings(List<User> users);
}
